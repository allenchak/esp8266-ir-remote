
///////////////////////////////////////////
//                                       //
// Standalone sketch, no need UNO/ATmega //
//                                       //
///////////////////////////////////////////

//Todo list: (Last Update: 2 Oct 2019)
// - [done] Check the user exists before add to pool
// - [done] debug: userupdate does not really clear the file
// - [done] debug: simple command sometimes not work (eg: /accmd 26)
// - [skip] temp command for delay process (Telegram has Schedule Message)
// - timeout reconnect logic
// - a new way to accept the master TGID / WiFi data (eg: intranet mode for config)


#include <FS.h>
#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include <TelegramBOT.h> //no need JSON lib, reduce ram and process time
#include <OneWire.h>
#include <MideaHeatpumpIR.h>       // https://github.com/ToniA/arduino-heatpumpir

//#define DEBUG
#define PIN_DS18B20      1 // TX, also work with D2
#define PIN_LED_IR       3 // RX

#define CFG_MAX_TGID    10 //how many TelegramID could using this remote
#define CFG_AC_MIN_TEMP 17  // Air Conditioner Minimum Temp
#define CFG_AC_MAX_TEMP 30  // Air Conditioner Maximum Temp
#define CFG_AC_TEMP     25  // Air Conditioner Default Temp: 17-30
#define CFG_AC_FAN       2  // Air Conditioner Default Fan Speed: 0: AUTO, 1: LOW, 2: MEDIUM, 3: HIGH


// get fingerprints from https://www.grc.com/fingerprints.htm
// Step to get the `fingerprint`, follow the website, `How to display SSL certificate fingerprint`
const char fingerprint[] PROGMEM = "BB DC 45 2A 07 E3 4A 71 33 40 32 DA BE 81 F7 72 6F 4A 2B 6B"; //for telegram


//String ssid, pass;
unsigned long allowTelegramIDs[ CFG_MAX_TGID ];  //unsigned long (Max: 4294967295)
char allowCnt = 0;
unsigned long rtnTGID, tgUpdateId;
bool manageCmd, masterSender, validSender = false;
byte addr[8] = { 0x28, 0x07, 0x00, 0x07, 0x6C, 0xD8, 0x01, 0x73 };
unsigned long getUpdatesMillis = 0;
unsigned long sendMsgIdMillis = 0;
bool configFileState = true;


WiFiClientSecure net_ssl;
TelegramBOT bot;
OneWire ds( PIN_DS18B20 );
IRSenderESP8266 irSender( PIN_LED_IR );
HeatpumpIR *heatpumpIR = new MideaHeatpumpIR();


void setup() {
  //Serial.begin(57600);
  //while( !Serial );
  //delay(500);

  allowTelegramIDs[0] = 21228303; //Allen (Master)
  //allowTelegramIDs[1] = 151884329; //Eric
  //allowTelegramIDs[2] = 203721834; //Allen 2nd
  //allowTelegramIDs[3] = 151420475; //Thomas
  //allowTelegramIDs[4] = 254387934; //Jac
  //allowTelegramIDs[5] = 0; //EricGF
  //allowTelegramIDs[6] = 0; //Suki
  //allowTelegramIDs[7] = 0; //William
  //allowTelegramIDs[8] = 0; //
  //allowTelegramIDs[9] = 0; //
  
  allowCnt = 1;

  WiFi.begin("WirelessGR_H", "12345678"); //Wifi SSID & Wifi Password
  while( WiFi.status() != WL_CONNECTED ){
    delay(500);
  }

  //net_ssl.setFingerprint(fingerprint); //magic code for ensure able to connect to Telegram Server
  bot.begin( F("886770959:AAF-7XV9S1dcob1VuJAA4459gEjlffnYiMs") ); //TelegramBot Token

  String content = "";
  content = content + F("Bot Woke, IP: ") + WiFi.localIP().toString() + F("%0A/userlist to list user.%0A/useradd to add user.%0A/userupdate to update users.");
  bot.sendMessage(String(allowTelegramIDs[0]), content, "");
  

  if (!SPIFFS.begin()){
    //unable to mount the FileSystem, please add user manually
    configFileState = false;
    alertToMasterConfigFileIssue( 101 );
  }else{
    //File system is ready
    loadUserFromFile();
  }
  
  //delay(100);
}


void alertToMasterConfigFileIssue( int issueType ){
  bot.sendMessage(String(allowTelegramIDs[0]), F("ESP01 - SPIFS - Config file is corrupt or damaged, considering reboot device."), "");
}

void loadUserFromFile(){
  File file = SPIFFS.open("/user.txt", "r");
  if (!file){
    //file not available, maybe not exists
    //alertToMasterConfigFileIssue(201);
  }else if (file.available()<=0){
    //file exists but not available
    //alertToMasterConfigFileIssue(202);
    file.close();
  }else{
    bool ended = false;
    while( !ended ){
      String strId = file.readStringUntil('\n');
      addUser( strId.toInt(), false );
      ended = ( file.position() + 1 >= file.size() );
    }
    file.close();
  }
}


void saveUserToFile( unsigned long tgId ){
  if( configFileState ){
    File file = SPIFFS.open("/user.txt", "a");
    if (!file){
      //File creation failed
      //alertToMasterConfigFileIssue(301);
    }else{
      file.println(tgId);
      file.flush();
      file.close();
    }
  }
}


void clearUserFile(){
  if( configFileState ){
    SPIFFS.remove("/user.txt");
  }
}


String readSerialByLine(){
  bool complete = false;
  String data;
  while( !complete ){
    if(Serial.available()){
      char c = (char)Serial.read();
      if( c == '\n'){
        complete = true;
      }else if( c != '\r' ){
        data += c;
      }
    }
  }
  return data;
}


void loop(){
  unsigned long currentMillis = millis();  
  if (currentMillis - getUpdatesMillis >= 2000L) { //2000L
    getUpdatesMillis = currentMillis;
    getUpdatesFromTelegram();
  }
}


void getUpdatesFromTelegram() {
  bot.getUpdates(bot.message[0][1]);
  for (int i = 1; i <= bot.message[0][0].toInt(); i++){
    rtnTGID = bot.message[i][2].toInt();
    validSender = false;
    masterSender = false;
    manageCmd = false;
    if( rtnTGID != allowTelegramIDs[0] ){
      //not a master, need find is valid user or not
      for( char j=1; j<allowCnt; j++ ){
        if( rtnTGID == allowTelegramIDs[j] ){
          validSender = true;
          break;
        }
      }
    }else{
      validSender = true;
      masterSender = true;
    }
    //(update_id, message_id, name_id, name, lastname, chat_id, text)
    
    if(validSender)
    {
      //admin action -- start
      /*
       * Commands
       * /userlist - list all of the users
       * /userupdate - update authorized user (all users will be deleted, then refill)
       * /useradd - add a single user
       */
      if(masterSender)
      {
        if( bot.message[i][6].startsWith("/userlist") ){
          manageCmd = true;
          String content = "";
          content = content + F("user:");
          for( char j=1; j<allowCnt; j++ ){
            content = content + packHtmlTgUserLink( String(allowTelegramIDs[j]) );
          }
          
          bot.sendMessage(String(allowTelegramIDs[0]), content, "");
          //bot.replyMessage(String(allowTelegramIDs[0]), content, bot.message[i][1]);
          
        }else if( bot.message[i][6].startsWith("/useradd") ){
          manageCmd = true;
          String temp = bot.message[i][6].substring(9);
          addUser(  temp.toInt(), true  );
          
          String content = "";
          content = content + F("%E2%9C%85 ID ") + temp + F(" added.");
          bot.replyMessage(String(allowTelegramIDs[0]), content, bot.message[i][1]);
          
        }else if( bot.message[i][6].startsWith("/userupdate") ){
          manageCmd = true;
          for( char j=1; j<allowCnt; j++ ){
            allowTelegramIDs[j] = 0;
          }
          allowCnt = 1;
          clearUserFile();
          
          String temp = bot.message[i][6];
          int m = temp.length();
          int n = 0;
          int p = 10; //length of "/userupdate"
          bool isFirst = false;
          yield();
          
          unsigned long tgId;
          for(;p+1<m;p++){
            if( (temp.substring(p, p+2) == "\\n" || p+2 == m) && !isFirst){
              isFirst = true;
              n = p+2;
            }else if(  temp.substring(p, p+2) == "\\n" ){
              tgId = temp.substring(n, p).toInt();
              addUser(  tgId, true  );
              n = p+2;
            }else if( p+2 == m ){
              tgId = temp.substring(n).toInt();
              addUser(  tgId, true  );
            }
          }
          
          bot.replyMessage(String(allowTelegramIDs[0]), F("%E2%9C%85"), bot.message[i][1]);
        }
      }
      //admin action -- end
      
      //normall user commands -- start
      if( !manageCmd ){
        if( bot.message[i][6].startsWith("/alive") ){
          bot.replyMessage(bot.message[i][5], F("%E5%A5%B4%E6%89%8D%E5%9C%A8"), bot.message[i][1]); //奴才在

        }else if( bot.message[i][6].startsWith("/roomtemp") ){
          String content = "";
          content = content + F("%E7%8F%BE%E5%9C%A8%E6%BA%AB%E5%BA%A6%EF%BC%9A"); //現在溫度：
          content = content + String(getRoomTempFromSensor());
          content = content + F("%C2%B0C"); //°C
          bot.replyMessage(bot.message[i][5], content, bot.message[i][1]);
          
        }else if( bot.message[i][6].startsWith("/accmd") ){
          if( bot.message[i][6].length() <= 7){
            bot.replyMessage(bot.message[i][5], F("%E6%BA%AB%E5%BA%A6%EF%BC%9A18-30%0A%E9%A2%A8%E9%80%9F%EF%BC%9AA / 1 / 2 / 3%0A%E6%A8%A1%E5%BC%8F%EF%BC%9A*C*ool / *D*ry / *H*eat / *F*an%0A%E4%BE%8B%E5%AD%90%EF%BC%9A `/accmd 25,1,C`%0A `/accmd 25`"), bot.message[i][1]);
          }else{
            processCustomAcCmd(i);
            String content = "";
            content = content + F("%E2%9C%85"); //Emoji, Green Tick
            content = content + String(bot.message[i][6]);
            bot.replyMessage(bot.message[i][5], content, bot.message[i][1]);
            //bot.replyMessage(bot.message[i][5], F("%E2%9C%85"), bot.message[i][1]); //Emoji, Green Tick
          }
        }else if( bot.message[i][6].startsWith("/acon") ){
          powerOn( CFG_AC_TEMP, CFG_AC_FAN, MODE_COOL );
          bot.replyMessage(bot.message[i][5], F("%E2%9C%85"), bot.message[i][1]); //Emoji, Green Tick
        }else if( bot.message[i][6].startsWith("/acoff") ){
          powerOff();
          bot.replyMessage(bot.message[i][5], F("%E2%9C%85"), bot.message[i][1]); //Emoji, Green Tick
        }else{
          //sendAsCommandToATmega(i);
          bot.replyMessage(bot.message[i][5], F("%F0%9F%9A%A7"), bot.message[i][1]); //Emoji, WIP
          
        }
      }
      //normall user commands -- end
    }
    else
    {
      //an invalid Telegram ID
      String content = "";
      content = content + F("New User ID") + packHtmlTgUserLink( String(bot.message[i][2]) );
      bot.sendMessage(String(allowTelegramIDs[0]), content, "");
    }
    debugPrintMsg(validSender, i);
  }
  bot.message[0][0] = ""; //reset counter
}


bool addUser( unsigned long tgId, bool saveToFile ){
  if( allowCnt >= CFG_MAX_TGID ){
    return false;
  }else{
    char idx = (allowCnt - 1);
    bool userExists = false;
    while (idx > 0){
      if( allowTelegramIDs[idx] == tgId )
        userExists = true;
      idx--;
    }

    if( !userExists ){
      allowTelegramIDs[allowCnt] = tgId;
      allowCnt++;
      if( saveToFile )
        saveUserToFile( tgId );
      return true;
    }else{
      return false;
    }
  }
}


String packHtmlTgUserLink( String tgId ){
  String rtn = "";
  rtn = rtn + F("%0A<a href=\"tg://user?id=") + tgId + F("\">") + tgId + F("</a>");
  return rtn;
}


void processCustomAcCmd(char botMsgIdx){
  char t, f, m, c, j, x, y, p = 0; //AC-Temp, AC-Fan, AC-Mode, char, Index, IndexX, IndexY, Pointer
  //String tmp = "/accmd ";
  int v = 0; //TmpValue
  x = bot.message[botMsgIdx][6].length();
  
  //set default value
  t = 25;
  f = 2; //FAN-2
  m = 3; //COOL

  for( j = 7; j < x; j++ ){
    c = bot.message[botMsgIdx][6].charAt(j);//toUpperCase();

    //toUpperCase
    if( c >= 'a' && c <= 'z' ){
      c = 'A' + ( c - 'a' );
    }
    
    if( c == 'A' ){
      v = 1;
      if( p == 1 )
        v = 0; //FAN_AUTO
    }else if( c == 'H' ){
      v = 2;
    }else if( c == 'C' ){
      v = 3;
    }else if( c == 'D' ){
      v = 4;
    }else if( c == 'F' ){
      v = 5;
    }else if( c >= '0' && c <= '9' ){
      v = (v * 10) + (int)( c - '0' );
    }

    
    if( c == ',' || ((j + 1) >= x) ){
      switch( p ){
        case 0:  t = v;  break;
        case 1:  f = v;  break;
        case 2:  m = v;  break;
      }
      p++;
      y=0;
      v=0;
    }
  } //end for

  //Serial.print("t:");
  //Serial.print(t, DEC);
  //Serial.print("; f:");
  //Serial.print(f, DEC);
  //Serial.print("; m:");
  //Serial.println(m, DEC);
  powerOn( t, f, m );

  String customCmdStr = " ";
  customCmdStr = customCmdStr + " " + String(t, DEC);
  customCmdStr = customCmdStr + "," + String(f, DEC);
  customCmdStr = customCmdStr + "," + String(m, DEC);
  bot.message[botMsgIdx][6] = customCmdStr;
  
  //tmp += (char)(t + 'A'); //Temperature (Celsius)
  //tmp += (char)(f + 'A'); //Fan Speed
  //tmp += (char)(m + 'A'); //Mode
}


void powerOn( char temp, char fan_speed, uint8_t ac_mode ){
  heatpumpIR->send(irSender, POWER_ON, ac_mode, fan_speed, temp, HDIR_AUTO, HDIR_AUTO);
}


void powerOff(){
  heatpumpIR->send(irSender, POWER_OFF, MODE_COOL, FAN_AUTO, 24, HDIR_AUTO, HDIR_AUTO);
}


void debugPrintMsg( bool validSender, char i ){
#ifdef DEBUG
  Serial.print("#[");
  Serial.print( i, DEC );
  Serial.print("] ");
  if( validSender ){
    Serial.print("BINGO !!!!!!!");
  }else{
    Serial.print("fromTgId: ");
    Serial.print( bot.message[i][2] );
    Serial.print(";");
  }
  Serial.print(" update_id: ");
  Serial.print( bot.message[i][0] );
  Serial.print("; msg_id: ");
  Serial.print( bot.message[i][1] );
  Serial.println(";");
  Serial.print(" text: ");
  Serial.print( bot.message[i][6] );
  Serial.println(";");
#endif
}


float getRoomTempFromSensor(){
  byte present = 0;
  byte data[12];
  float celsius;

  ds.reset();
  ds.select(addr);
  ds.write(0x44, 1);        // start conversion, with parasite power on at the end
  
  delay(750);
  //delay(1000);     // maybe 750ms is enough, maybe not
  // we might do a ds.depower() here, but the reset will take care of it.
  
  present = ds.reset();
  ds.select(addr);
  ds.write(0xBE);         // Read Scratchpad
  
  for ( char i = 0; i < 9; i++) {
    data[i] = ds.read();
  }
  
  int16_t raw = (data[1] << 8) | data[0];

  byte cfg = (data[4] & 0x60);
  // at lower res, the low bits are undefined, so let's zero them
  if (cfg == 0x00) raw = raw & ~7;  // 9 bit resolution, 93.75 ms
  else if (cfg == 0x20) raw = raw & ~3; // 10 bit res, 187.5 ms
  else if (cfg == 0x40) raw = raw & ~1; // 11 bit res, 375 ms
  //// default is 12 bit resolution, 750 ms conversion time

  celsius = (float)raw / 16.0;
  return celsius;
}


