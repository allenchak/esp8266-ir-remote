/*
  Copyright (c) 2015 Giancarlo Bacchio. All right reserved.

  TelegramBot - Library to create your own Telegram Bot using 
  ESP8266 on Arduino IDE.
  Ref. Library at https:github/esp8266/Arduino

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
  */


#ifndef TelegramBOT_h
#define TelegramBOT_h

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>

class TelegramBOT
{
	public:
		TelegramBOT ();
		String message[3][7];  // amount of messages read per time  (update_id, message_id, name_id, name, lastname, chat_id, text)
		void begin(String token);
		void analizeMessages(void);
    void sendMessage(String chat_id, String text, String reply_markup);
    void replyMessage(String chat_id, String text, String reply_msg_id);
		void getUpdates(String offset);
		const char* fingerprint = "BB:DC:45:2A:07:E3:4A:71:33:40:32:DA:BE:81:F7:72:6F:4A:2B:6B";  //Telegram.org Certificate 

	private:
		String connectToTelegram(String command);
		String _token;
		String _name;
		String _username;
		WiFiClientSecure client;
};

#endif
